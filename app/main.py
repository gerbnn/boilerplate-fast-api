
from fastapi import FastAPI
from app.routers import data, health
from app.utilities.http_logger import LogRequestToDatabaseMiddleware
from app.config import settings
from fastapi.middleware.cors import CORSMiddleware

is_production = settings.IS_PRODUCTION

if is_production:
    docs_url = None
    redoc_url = None
else:
    docs_url = "/docs"
    redoc_url = "/redoc"

app = FastAPI(
    title="NLP API",
    description='description',
    version="0.0.1",
    terms_of_service="https://kixs.mindef.nl",
    contact={
        "name": "Datalab",
        "url": "https://datalab.nl",
        "email": "datalab@mindef.nl",
    },
    license_info={
        "name": "All rights reserved",
        "url": "https://kixs.mindef.nl",
    },
    docs_url=docs_url,
    redoc_url=redoc_url
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.add_middleware(LogRequestToDatabaseMiddleware)
app.include_router(data.router, tags=['Data'], prefix='/v1/data')
app.include_router(health.router, tags=['Health'], prefix='/v1/health')