from fastapi import APIRouter
from fastapi.responses import StreamingResponse
import pandas as pd
from .. import schemas


router = APIRouter()

@router.post('/keywords', response_model=schemas.GenericResponse)
def get_keywords(wav_audio: str):

    reponse_object = {
        'data': [{}],
        'message': 'Test',
        'total_records': 1,
        'status': 'OK'
    }
    return reponse_object