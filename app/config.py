from pydantic import BaseSettings


class Settings(BaseSettings):
    DATABASE_URL: str
    MONGO_INITDB_DATABASE: str
    IS_PRODUCTION: bool

    class Config:
        env_file = './.env'


settings = Settings()
