from fastapi import Request
from starlette.middleware.base import BaseHTTPMiddleware
import datetime
import time
from app.database import http_logging


class LogRequestToDatabaseMiddleware(BaseHTTPMiddleware):
    def __init__(
            self,
            app
    ):
        super().__init__(app)

    async def dispatch(self, request: Request, call_next):
        # Before request
        start_time = time.time()

        # Process the request and get the response
        response = await call_next(request)

        # Check if app is behind a proxy
        proxy_ip = request.headers.get('X-Forwarded-For')
        if proxy_ip:
            user_ip = proxy_ip
        else:
            user_ip = request.client.host

        log = {
            "method": request.method,
            "url": request.url.path,
            "query_params": dict(request.query_params),
            "path_params": dict(request.path_params),
            "referer": request.url.hostname,
            "remote_ip": user_ip,
            "user_ip": request.client.host,
            "user_agent": request.headers.get('User-Agent'),
            "status": str(response.status_code),
            "created": datetime.datetime.now(),
        }

        # Calculate processing time
        pt = time.time() - start_time
        log["process_time"] = str(pt)

        # Save to database
        http_logging.insert_one(log)

        return response
