# Boilerplate API based on FastAPI

## Development

Run the api locally using ```uvicorn``` with auto reload enabled.

### Create venv

~~~bash
python3 -m venv venv
~~~

### Install dependencies

~~~bash
pip install -r requirements.txt
~~~

### Run server

~~~bash
uvicorn app.main:app --host localhost --port 8000 --reload
~~~

## Production

This git repository is checked out on the remote production server.

~~~bash
git pull origin main
~~~

### Build container

~~~bash
docker build -t nlp .
~~~

### Run compose up

~~~bash
docker-compose up -d
~~~